from django.db import models
from django import forms

class Room(models.Model):
    STAGES = (
        (1, 'Gathering Data'),
        (2, 'Discussion'),
        (3, 'Proving Knowledge'),
    )
    room_id = models.AutoField(primary_key=True)
    room_password = models.CharField(max_length=50)
    room_currentPersonId = models.IntegerField(null=True)
    room_currentStage = models.IntegerField(choices=STAGES, default=1)
    room_started = models.BooleanField(default=False)
    room_complete = models.BooleanField(default=False)


    def __str__(self):
        return "Room " + str(self.room_id)

class Participant(models.Model):
    p_id = models.AutoField(primary_key=True)
    p_name = models.CharField(max_length=50)
    p_room = models.ForeignKey(Room, on_delete=models.CASCADE, null=True)
    p_wasTarget = models.BooleanField(default=False)
    p_score = models.IntegerField(default=0)

    def __str__(self):
        return str(self.p_name) + " | " + str(self.p_id)

class Question(models.Model):
    question_id = models.AutoField(primary_key=True)
    question_content = models.CharField(max_length=255)
    question_alt_content = models.CharField(max_length=255)

    def __str__(self):
        return "Question " + str(self.question_id)

class RoomQuestion(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)

class Answer(models.Model):
    a_id = models.AutoField(primary_key=True)
    a_question = models.ForeignKey(Question, on_delete=models.CASCADE, null=True)
    a_participant = models.ForeignKey(Participant, on_delete=models.CASCADE, null=True)
    a_room = models.ForeignKey(Room, on_delete=models.CASCADE, null=True)
    a_content = models.CharField(max_length=255, null=True)
