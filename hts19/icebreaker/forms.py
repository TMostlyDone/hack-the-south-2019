from django import forms
from icebreaker.models import *

class AddParticipantForm(forms.Form):
    fname = forms.CharField(label="enter your name", max_length=100, required=True)

class JoinRoomForm(forms.Form):
    room_id = forms.CharField(max_length=6, min_length=6, required=True)

class AnswerQuestionsForm(forms.ModelForm):
    class Meta:
        model = Answer
        fields = ['a_question', 'a_room', 'a_content', 'a_participant']
        widgets = {'a_question': forms.HiddenInput(), 'a_room': forms.HiddenInput(), 'a_participant': forms.HiddenInput()}

class ThirdStageForm(forms.Form):
    a_content = forms.CharField(max_length=255, required=True, widget=forms.HiddenInput())
    u_content = forms.CharField(max_length=255, required=False)
    a_room = forms.CharField(min_length=6, max_length=6, required=True, widget=forms.HiddenInput())
