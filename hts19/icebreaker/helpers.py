from icebreaker.models import *
import urllib, random

class HelperMethods:

    def escape_url(url_to_escape):
        return urllib.parse.quote_plus(url_to_escape)
        
    def un_escape_url(url_to_unescape):
        return urllib.parse.unquote_plus(url_to_unescape)

    def get_random_user_for_room(room_id):
        room = Room.objects.get(room_password=room_id)
        people = Participant.objects.filter(p_room=room, p_wasTarget=False)
        if (people==None or len(people) == 0):
            return None
        user_index = random.randint(1, len(people)) - 1
        return people[user_index]
