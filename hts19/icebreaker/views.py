from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.template.loader import get_template
from django.views.decorators.http import require_http_methods
from icebreaker.models import *
from icebreaker.forms import *
from icebreaker.helpers import HelperMethods

import random, string, urllib

# Create your views here.

def welcome(request):
    return render(request, 'icebreaker/welcome.html')

def join_room(request, room_id=None):
    if (request.method == "POST"):
        try:
            form = JoinRoomForm(request.POST)
            if (form.is_valid()):
                room_id = form.cleaned_data['room_id']
                print("Room ID: " +  room_id)
                room = Room.objects.get(room_password=room_id)
        except:
            return redirect('welcome')
        
    if not request.session.get('logged_in_user', False):
        return redirect('participate', n=HelperMethods.escape_url('/join_room/' + room_id))    
    else:
        user = Participant.objects.get(p_id=request.session.get('logged_in_user'))
        user.p_room = Room.objects.get(room_password=room_id)
        user.save()
        return redirect('room', room_id=room_id)

def logout(request):
    try:
        del request.session['logged_in_user']
        del request.session['stage']
        del request.session['question']
        del request.session['person']
        del request.session['stage_score']

    except KeyError:
        pass
    return render(request, 'icebreaker/logout.html')


@require_http_methods(['POST'])
def save_answer(request):
    form = AnswerQuestionsForm(request.POST)
    if (form.is_valid()):
        # Add Answer
        Answer.objects.create(a_question=form.cleaned_data['a_question'], a_room=form.cleaned_data['a_room'], a_participant=form.cleaned_data['a_participant'], a_content=form.cleaned_data['a_content'])
        return redirect('room', room_id=form.cleaned_data['a_room'].room_password)

@require_http_methods(['POST'])
def check_answer(request):
    form = ThirdStageForm(request.POST)
    if (form.is_valid()):
        current_score = request.session.get('stage_score',0)
        # Check Answer
        if form.cleaned_data['a_content'] == form.cleaned_data['u_content']:
            current_score = current_score + 1
        request.session['stage_score'] = current_score
        return redirect('room', room_id=form.cleaned_data['a_room'])


def create_room(request):

    print(request.session.get('logged_in_user'))
    if not request.session.get('logged_in_user', False):
        return redirect('participate', n="create_room")

    # Try to create a new room
    try:
        room_code = ''.join(random.choices(string.ascii_letters + string.digits, k=6))
        print(room_code)
        new_room = Room.objects.create(room_password=room_code)
        creating_user = Participant.objects.get(p_id=request.session.get('logged_in_user'))
        creating_user.p_room = new_room
        creating_user.save()
        # Gen Questions
        total_questions = Question.objects.all()
        sample_size = 3
        sample_questions = [
            total_questions[i] for i in sorted(random.sample(range(len(total_questions)), sample_size))
        ]
        for question in sample_questions:
            RoomQuestion.objects.create(room=new_room, question=question)
        return redirect("room", room_id=new_room.room_password)
    except Exception as e:
        return HttpResponse(e)

def room(request, room_id, stage_number=0, question_number=0):

    # Check if room exists
    try:
        room = Room.objects.get(room_password=room_id)
        if room.room_currentStage != stage_number:
            return redirect('updates', room_id=room_id)
        if room.room_complete:
            return redirect('room_end', room_id=room_id)
    except Exception as e:
        return HttpResponse(e)

    seshStage = room.room_currentStage
    print(str(seshStage) + " | " + str(stage_number))
    seshQuest = request.session.get('question',0)
    if (stage_number != seshStage or question_number != seshQuest):
        return redirect('room', room_id=room_id, 
                stage_number=room.room_currentStage,
                question_number=request.session.get('question',0))
    
    room = Room.objects.get(room_password=room_id)
    people = Participant.objects.filter(p_room=room)
    try:
        focused = people.get(p_id=room.room_currentPersonId).p_name
    except:
        focused = "Game not started"

    default_context = {
        "people": people,
        "stage_number": stage_number,
        "question_number": 0,
        "focused_user": focused,
        "room_code": room_id
    }


    # Logic for Stage 1
    if stage_number == 1 and room.room_started:
        your_session_id = request.session.get('logged_in_user')
        person = request.session.get('person')
        if (your_session_id != person):
            default_context = default_context.update( {"message": "Waiting for Player To Answer Questions"})
            print(your_session_id)
            return render(request, 'icebreaker/lobby.html', default_context)
        else:
            if (question_number == 0):
                request.session['question'] = 1
                question_number=1
            roomQuestions = RoomQuestion.objects.filter(room=room)
            questions = []
            for rq in roomQuestions:
                print(rq)
                questions.append(rq.question)
            if (len(questions) < question_number):
                return redirect('advance_room', room_id=room_id)
            question_content=questions[question_number-1].question_content
            request.session['question'] = request.session['question'] + 1
            form = AnswerQuestionsForm(initial={
                'a_question': questions[question_number-1],
                'a_room': room,
                'a_participant': people.get(p_id=your_session_id)})
            default_context.update({
                "form": form,
                "question_number": question_number,
                "question_content": question_content})
            return render(request, 'icebreaker/stage1.html', default_context)
    elif stage_number == 2:
        if (request.session.get('logged_in_user') != request.session.get('person')):
            default_context.update({"focused": False})
        else:
            default_context.update({"focused": True})
        return render(request, 'icebreaker/stage2.html', default_context) 
    elif stage_number == 3:
        print(request.session.get('person'))
        focused_person = Participant.objects.get(p_id=request.session.get('person'))
        if (request.session.get('logged_in_user') == request.session.get('person')):
            default_context.update(
                    {"message": "Waiting for Players to Answer Questions",
                     "stage": 3}
                    )
            return render(request, 'icebreaker/lobby.html', default_context)
        else:
            if (question_number == 0):
                request.session['question'] = 1
                question_number=1
            answers = Answer.objects.filter(a_participant=focused_person, a_room=room)
            question_answers = {}
            questions = []
            for answer in answers:
                answerQ = answer.a_question
                answerContent = answer.a_content
                question_answers[answerQ] = answerContent
                questions.append(answerQ)
            if (len(question_answers) < question_number):
                you = Participant.objects.get(p_id=request.session.get('logged_in_user'))
                you.p_score = request.session.get('score',0)
                you.save()
                return render(request, 'icebreaker/lobby.html', default_context)
            question = questions[question_number-1]
            question_content = question.question_alt_content
            request.session['question'] = request.session['question'] + 1
            form = ThirdStageForm(initial={
                'a_content': question_answers[question],
                'a_room': room_id})
            default_context.update({
                "form": form,
                "question_number": question_number,
                "question_content": question_content})
            return render(request, 'icebreaker/stage3.html', default_context) 
    else:
        default_context.update({"pregame": True})
        return render(request, 'icebreaker/lobby.html', default_context)

def room_end(request,room_id):
    room = Room.objects.get(room_password=room_id)
    user = Participant.objects.get(p_id=request.session.get('logged_in_user'))
    user.p_score = request.session.get('score', 0)
    user.save()
    if (room.room_complete):
        people_in_room = Participant.objects.filter(p_room = room) 
        context = {
            "people": people_in_room
        }
        return render(request, 'icebreaker/roomend.html', context)
    else:
        return redirect('room', room_id=room_id)

def check_for_room_updates(request, room_id):
    seshStage = request.session.get('stage', None)
    room = Room.objects.get(room_password=room_id)
    if (room.room_currentStage != seshStage):
        request.session['stage'] = room.room_currentStage
        request.session['question'] = 0
        request.session['person'] = room.room_currentPersonId
    return redirect('room', room_id=room_id, stage_number=room.room_currentStage, question_number=0)

def add_participant(request, n="welcome"):
    
    n = HelperMethods.un_escape_url(n)
    print(n)
    if request.session.get('logged_in_user', False):
        return redirect('welcome')
    
    if (request.method == "POST"):
        try:
            form = AddParticipantForm(request.POST)
            if (form.is_valid()):
                fname = form.cleaned_data['fname']
                nuser = Participant.objects.create(p_name=fname)
                request.session['logged_in_user'] = nuser.p_id
                return redirect(n)
        except Exception as e:
            return redirect(n)
    else:
        return render(request, 'icebreaker/add_participant.html')

def advance_room(request, room_id=None):
    
    try:
        room = Room.objects.get(room_password=room_id)
        if (room.room_complete):
            redirect('room_end', room_id=room.room_password)
    except:
        return redirect('welcome')

    # Increase stage
    if room.room_currentStage < 3 and room.room_started:
        room.room_currentStage = room.room_currentStage + 1
    else:
        try:# Remove current focused user
            current_focused = Participant.objects.get(p_id=room.room_currentPersonId)
            current_focused.p_wasTarget=True
            current_focused.save()
        except:
            pass
        focused_user = HelperMethods.get_random_user_for_room(room_id)
        room.room_currentStage = 1
        if focused_user != None:
            room.room_currentPersonId = focused_user.p_id
        else:
            room.room_currentPersonId = None
        room.room_started = True

    # Save room
    room.save()
    room = Room.objects.get(room_password=room_id)
    
    # Set Session Stuff
    request.session['stage'] = room.room_currentStage
    request.session['question'] = 0
    request.session['person'] = room.room_currentPersonId

    # End Game Logic
    if (room.room_currentPersonId == None):
        room.room_complete = True
        room.save()
        return redirect('room_end', room_id=room_id)

    return redirect('room', room_id=room_id)



    

