from django.contrib import admin
from icebreaker.models import *

# Register your models here.
admin.site.register(Room)
admin.site.register(Participant)
admin.site.register(Question)
admin.site.register(RoomQuestion)
admin.site.register(Answer)
