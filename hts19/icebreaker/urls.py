from django.urls import path

from . import views

urlpatterns = [
    path('', views.welcome, name="welcome"),
    path('create_room', views.create_room, name="create_room"),
    path('join_room', views.join_room, name="join_room"),
    path('join_room/<str:room_id>', views.join_room, name="join_room"),
    path('room/<str:room_id>', views.room, name="room"),
    path('room/<str:room_id>/<int:stage_number>/<int:question_number>', views.room, name="room"),
    path('advance_room/<str:room_id>', views.advance_room, name="advance_room"),
    path('participate', views.add_participant, name="participate"),
    path('participate/<str:n>', views.add_participant, name="participate"),
    path('logout', views.logout, name="logout"),
    path('check_for_room_updates/<str:room_id>', views.check_for_room_updates, name="updates"),
    path('save_answer', views.save_answer, name="save_answer"),
    path('room_end/<str:room_id>', views.room_end, name="room_end"),
    path('check_answer', views.check_answer, name="check_answer")
]
