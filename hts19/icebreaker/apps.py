from django.apps import AppConfig


class IcebreakerConfig(AppConfig):
    name = 'icebreaker'
