# Hack the South 2019

## Team 
- Jonathan <jsb1g18@soton.ac.uk>
- Julian <ja1g18@soton.ac.uk>
- Matt <mt2g18@soton.ac.uk>

## The Project

### Basic Flow
1. One person navigates to the site and creates a room
2. They share the code with other users who join from the home page
3. When everybody is in the lobby the game can start when somebody presses the button
4. Everyone should refresh (see improvements below)
5. One user in the group will be asked questions, when the questions are finished they will be redirected to a Discussion Page.
6. Everyone should refresh (see improvements below)
7. Discuss about the questions and when finished click the butter if you answered in stage 1
8. Everyone refresh.
9. All users except person from stage 1 now answers questions about the focused person
10. When everyone has finished, the focused person can click the finish button
11. Everyone refresh and repeat steps 4-10 for each person
12. Game Ended Page Shows

### Known Issues
- Scoring is not working
- Database needs to be cleared periodically
- Manual refreshing is required (auto refreshing occurs every 5 seconds on lobby pages)
- Stage 1 is broken if not focused (Not a major problem but inconvenient)

### Improvement Suggestions
- Scoring that works
- Auto refreshing of content through AJAX
- Timed mode
- Output a list of all questions and answers at the end
- Larger number of questions (possibly customisable)
- Custom Question Sets
